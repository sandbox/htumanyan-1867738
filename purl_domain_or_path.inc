<?php

class purl_domain_or_path implements purl_processor {
  protected $_processor;

  protected function get_processor($element){
	$hasDot = strchr($element, '.');
	if ($hasDot)
	  return purl_get_processor('domain');

    return purl_get_processor('path');
  }

  public function method(){
	return 'domain_or_path';
  }

  public function admin_form(&$form, $id){
  }

  public function description(){
	return t('Choose a path or domain. Paths may contain only lowercase letters, numbers, dashes and underscores. Domains must contain, at least one dot');
  }

  public function detect($q){
	$this->_processor = $this->get_processor($q);
//var_dump($this->_processor); exit;
	return $this->_processor->detect($q);
  }

  public function parse($valid_values, $value){
//var_dump($valid_values); exit;
	return $this->_processor->parse($valid_values, $value);
  }

  public function adjust(&$value, $element, &$q){
	return $this->_processor->adjust($value, $element, $q);
  }

  public function rewrite(&$path, &$options, $element){
	$this->_processor = $this->get_processor($element->value);
	return $this->_processor->rewrite($path, $options, $element);
  }
}

?>